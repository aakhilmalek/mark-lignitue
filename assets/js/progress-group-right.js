$(document).ready(function() {

    $(function () {

        $(window).bind("resize", function () {
            // console.log($(this).width())
            if ($(this).width() >= 1025) {
                $("#progress-group-right-button").click(function(){
                    
                    $(".right-side-nav").toggleClass("collapse-menu collapse-menu-disabled");
                    $(".header-dashboard").toggleClass("collapsed-header");
                    $(".main-dashboard").toggleClass("dashboard-expand");
                    $("body").toggleClass("mini-navbar");
                    $(".right-side-menu").addClass("text-fixed-remove");
                    setTimeout(function() {
                        $(".right-side-menu").removeClass("text-fixed-remove");
                    }, 200);
            
                    $(".progress-group-fixed-root").toggleClass("active");
                    $(".main-middle-area").toggleClass("active-progress-area");
                    $(".backdrop-progress").toggleClass("active-backdrop-progress");

                    $(".active-backdrop-progress").click(function(){
                        $(".right-side-nav").removeClass("collapse-menu collapse-menu-disabled");
                        $(".header-dashboard").removeClass("collapsed-header");
                        $(".main-dashboard").removeClass("dashboard-expand");
                        $("body").removeClass("mini-navbar");
                        $(".right-side-menu").removeClass("text-fixed-remove");
                        setTimeout(function() {
                        $(".right-side-menu").removeClass("text-fixed-remove");
                        }, 200);
                
                        $(".progress-group-fixed-root").removeClass("active");
                        $(".main-middle-area").removeClass("active-progress-area");
                        $(".backdrop-progress").removeClass("active-backdrop-progress");
                    });
                });
            } else {
                $("#progress-group-right-button").click(function(){
                    $("body").toggleClass("mini-navbar");
                    $(".progress-group-fixed-root").toggleClass("active");
                    $(".main-middle-area").toggleClass("active-progress-area");
                });
            }
        }).resize();
    });
});