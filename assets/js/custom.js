$(document).ready(function(){ 

  /* mobile menu */
  $("#menu-open").click(function(){
    $("#dashboard-menu-clone").addClass("active");
    $(".right-side-nav").addClass("active");
  });

  $("#menu-closed").click(function(){
    $("#dashboard-menu-clone").removeClass("active");
    $(".right-side-nav").removeClass("active");
  });
  
  /* End of mobile menu */

  /* Desktop collapse menu */
  $("#collapse-nav").click(function(){
    $(this).toggleClass("collapsed-menu");
    $(".right-side-nav").toggleClass("collapse-menu");
    $(".header-dashboard").toggleClass("collapsed-header");
    $(".main-dashboard").toggleClass("dashboard-expand");
    $("body").toggleClass("mini-navbar");
    $(".right-side-menu").addClass("text-fixed-remove");
    setTimeout(function() {
      $(".right-side-menu").removeClass("text-fixed-remove");
    }, 200);
  });
  /* End of desktop collapse menu */

  $("[data-toggle='tooltip']").tooltip();

});