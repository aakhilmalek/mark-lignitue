$(document).ready(function() {

    /* select 2 */
    $(".js-select2").select2({
        placeholder: "Type here",
        dropdownPosition: "below",
    });

    /* End of select 2 */

    /* profile pic */
    var readURL = function(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $(".profile-pic").attr("src", e.target.result);
            }
    
            reader.readAsDataURL(input.files[0]);
        }
    }
    
    $(".file-upload").on("change", function(){
        readURL(this);
    });
    
    $(".upload-button").on("click", function() {
        $(".file-upload").click();
    });

    /* end of pic */

    /* Password */

    $("#show_password01").on("click", function(){  
        var passwordField1 = $("#password01");  
        var passwordFieldType1 = passwordField1.attr("type");
        
        if(passwordField1.val() != "")
        {
            if(passwordFieldType1 == "password")  
            {  
                passwordField1.attr("type", "html");  
                $(this).html('<span class="fe fe-eye-off pass-custom-icon password-hide"></span>');
            }  
            else  
            {  
                passwordField1.attr("type", "password");  
                $(this).html('<span class="fe fe-eye pass-custom-icon password-view"></span>');     
            }
        }
    }); 

    $("#show_password02").on("click", function(){  
        var passwordField2 = $("#password02");  
        var passwordFieldType2 = passwordField2.attr("type");
        
        if(passwordField2.val() != "")
        {
            if(passwordFieldType2 == "password")  
            {  
                passwordField2.attr("type", "html");  
                $(this).html('<span class="fe fe-eye-off pass-custom-icon password-hide"></span>');
            }  
            else  
            {  
                passwordField2.attr("type", "password");  
                $(this).html('<span class="fe fe-eye pass-custom-icon password-view"></span>');     
            }
        }
    }); 

    $("#show_password03").on("click", function(){  
        var passwordField3 = $("#password03");  
        var passwordFieldType3 = passwordField3.attr("type");
        
        if(passwordField3.val() != "")
        {
            if(passwordFieldType3 == "password")  
            {  
                passwordField3.attr("type", "html");  
                $(this).html('<span class="fe fe-eye-off pass-custom-icon password-hide"></span>');
            }  
            else  
            {  
                passwordField3.attr("type", "password");  
                $(this).html('<span class="fe fe-eye pass-custom-icon password-view"></span>');     
            }
        }
    }); 

    /* End of password */

});